************************************
version.module for Drupal 4.7.x

Writen by drupal user robertgarrigos

Updated to the new form API 
by Harry Slaughter
************************************

This module list all files of your drupal installation with their '// $Id:' line.

Additionally, if you insert you own id line in the drupal files you patch, lilke I do, you can have this line listed also. Make sure you insert this extra line just next to the drupal one, this is, the 3rd line of the file.

Go to the settings page for this module to set the full path of your drupal installation and any extra configuration available.