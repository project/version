<?php

// $Archive: /homepages/masticscum/include/dir.php $
// $Author$
// $Date$
// $Revision$

/** Class for reading a directory structure.
aFiles contains multiple aFile entries
aFile:   Path        => relative path eg. ../xx/yy/
         File        => filename eg. filename (without extension)
         Extension   => ext
         IsDirectory => true/false
         FullName    => Path . File . '.' . Extension
         FileName    => File . '.' . Extension

Notes
Filenames with multiple Extensions: only the last extensions is saved as extensions
eg: aaa.bbb.ccc results in File=aaa.bbb and Extension=ccc
Filenames are stored in the same case as the are stored in the filesystem
sFilter is only applied to files.
*/

Class CDir
{
    var $aFiles;
	var $fVerbose;
	var $fCasesensitiv = true;
	var $fLowercase = false;

    /** <b>Constructor</b><br>
    	@param fVerbose = false display trace information
    */
    Function CDir( $fVerbose = false )
    {
	    $this->fVerbose = $fVerbose;
		$this->Init();
    }

    /** Initialize Class
    */
    Function Init()
    {
        unset( $this->aFiles );
        $this->aFiles = array();
    }

	/** Output messages to screen with echo
		@param sText
	*/
	Function Log( $sText )
	{
	    if ( $this->fVerbose )
	    {
	        echo $sText;
	        flush();
        }
	}

    /** reads directories and filenames
    	@param $sPath string path eg. '../xx/yy/' (please notice the last '/')
    	@param $sInclude string regular expression for filtering directories and filenames
    	@param $fRecursive bool read subdirectories as well
    	@param $fFiles bool include files
    	@param $fDirectory bool include directories
    	@param $sRoot string prepend root directory string (good for converting filesystem paths to urls)
    	@param $sExclude string regular expression for excluding files and directories
    */
    Function Read( $sPath, $sInclude = '', $fRecursive = false, $fFiles = true, $fDirectories = true, $sRoot = '', $sExclude = '' )
    {
        $this->Log( "Path: $sPath<br>\n" );
        $this->Log( "Include: $sInclude<br>\n" );
        $this->Log( "Recursive: $fRecursive<br>\n" );
        $this->Log( "Files: $fFiles<br>\n" );
        $this->Log( "Directories: $fDirectories<br>\n" );
        $this->Log( "Root: $sRoot<br>\n" );
        $this->Log( "Exclude: $sExclude<br>\n" );

        $aFiles = array();
		$oHandle = opendir( $sPath );
        while ( ( $sFilename = readdir( $oHandle ) ) !== false )
        {
			if ( $sFilename == '.' || $sFilename == '..' )
                continue;
            $aFiles[] = $sFilename;
        }
        closedir( $oHandle );

        foreach( $aFiles as $sFilename )
        {
			$sFullname = $sRoot . $sFilename;
            $fInsert = true;
            $fIsDirectory = is_dir( $sPath . $sFilename );

            $fExclude = false;
			if ( !empty( $sExclude ) )
			{
			    if ( $this->GetCasesensitiv() )
			        $fExclude = ereg( $sExclude, $sFullname );
			    else
			        $fExclude = eregi( $sExclude, $sFullname );
                if ( $fExclude )
                {
					$this->Log( "Excluded: $sFullname<br>\n" );
					$fInsert = false;
                }
            }

            $fInclude = true;
			if ( !empty( $sInclude ) && !$fIsDirectory )
            {
			    if ( $this->GetCasesensitiv() )
			        $fInclude = ereg( $sInclude, $sFullname );
			    else
			        $fInclude = eregi( $sInclude, $sFullname );
                if ( !$fInclude )
                {
					$this->Log( "Not Included: $sFullname<br>\n" );
                    $fInsert = false;
                }
            }

            if ( !$fFiles && !$fIsDirectory )
                $fInsert = false;
            if ( !$fDirectories && $fIsDirectory )
                $fInsert = false;


            if ( $fInsert )
            {
				$this->Log( "Ok: $sFullname<br>\n" );

				$i = strrpos( $sFilename, '.' ) + 1;
                if ( substr( $sFilename, $i - 1, 1 ) == '.' )
                {
                    $sFile = substr( $sFilename, 0, $i - 1 );
                    $sExtension = substr( $sFilename, $i );
                }
                else
                {
                    $sFile = $sFilename;
                    $sExtension = '';
                }

                if ( $this->GetLowercase() )
                {
                    $sFile = strtolower( $sFile );
                    $sExtension = strtolower( $sExtension );
                }

                $aFile = array
                    (
                        'Path' => $sRoot,
                        'File' => $sFile,
                        'Extension' => $sExtension,
                        'Filename' => $sFilename,
                        'Fullname' => $sRoot . $sFilename,
                        'IsDirectory' => $fIsDirectory
                    );

                // Insert current file into aFiles array
                $this->aFiles[] = $aFile;
            }

            // Recursion?
            if ( $fRecursive && $fIsDirectory && !$fExclude )
            {
                $this->Log( "Rekursion: $sPath$sFilename/<br>\n" );
                $this->Read( $sPath . $sFilename . '/', $sInclude, $fRecursive, $fFiles, $fDirectories, $sRoot . $sFilename . '/', $sExclude );
            }
        }
    }
    
    /** Returns number of files/directories found
        return int
    */
    Function Count()
    {
        return( count( $this->aFiles ) );
    }
    
    /** Sorts array of found items
        param $sKey Key to sort by: File, Extension, Filename, Fullname
        param $fAscending = true
    */
    Function Sort( $sKey, $fAscending )
    {
        foreach( $this->aFiles as $aFile )
            $aSort[] = $aFile[ $sKey ];
        
        if ( $fAscending )
            array_multisort( $aSort, $this->aFiles );
        else
            array_multisort( $aSort, SORT_DESC, $this->aFiles );
    }

    /** outputs everything found (good for debugging)
    */
    Function Output()
    {
        echo 'Number of Items found: ' . $this->Count() . "<br>\n";
        echo "<hr>\n";
        foreach( $this->aFiles as $aFile )
            $this->OutputFile( $aFile );
    }

    /** outputs everything of a file entry (good for debugging)
    	@param aFile File entry
    */
    Function OutputFile( $aFile )
    {
        printf( "Path: %s<br>\n", $this->GetPath( $aFile ) );
        printf( "File: %s<br>\n", $this->GetFile( $aFile ) );
        printf( "Extension: %s<br>\n", $this->GetExtension( $aFile ) );
        printf( "IsDirectory: %s<br>\n", $this->GetIsDirectory( $aFile ) ? 'true' : 'false' );
        printf( "IsFile: %s<br>\n", $this->GetIsFile( $aFile ) ? 'true' : 'false' );
        printf( "Filename: %s<br>\n", $this->Filename( $aFile ) );
        printf( "Directoryname: %s<br>\n", $this->Directoryname( $aFile ) );
        printf( "Fullname: %s<br>\n", $this->Fullname( $aFile ) );
        echo "<hr>\n";
    }

    /** returns the path of a file (or directory)
    	@param aFile File entry
    	@return string
    */
    Function GetPath( $aFile )
    {
        return( $aFile[ 'Path' ] );
    }

    /** returns the filename without the extension of a file (or directory)
    	@param aFile File entry
    	@return string
    */
    Function GetFile( $aFile )
    {
        return( $aFile[ 'File' ] );
    }

    /** returns the extension of a file (or directory)
    	@param aFile File entry
    	@return string
    */
    Function GetExtension( $aFile )
    {
        return( $aFile[ 'Extension' ] );
    }

    /** returns true, if entry is a directory
    	@param aFile File entry
    	@return bool
    */
    Function GetIsDirectory( $aFile )
    {
        return( $aFile[ 'IsDirectory' ] );
    }

    /** returns true, if entry is a file
    	@param aFile File entry
    	@return bool
    */
    Function GetIsFile( $aFile )
    {
        return( !$this->GetIsDirectory( $aFile ) );
    }

    /** Returns Filename or Directory name (including ending '/')
    	@param aFile File entry
    	@return string
    */
    Function Filename( $aFile )
    {
        if ( $this->GetIsDirectory( $aFile ) )
            return( $aFile[ 'Filename' ] . '/' );
        else
            return( $aFile[ 'Filename' ] );
    }

    /** Directoryname returns the same as Filename, but without a ending '/' for Directories.
    	@param aFile File entry
    	@return string
    */
    Function Directoryname( $aFile )
    {
        return( $aFile[ 'Filename' ] );
    }

    /** Returns Fullname (path and filename)
    	@param aFile File entry
    	@return string
    */
    Function Fullname( $aFile )
    {
        if ( $this->GetIsDirectory( $aFile ) )
            return( $aFile[ 'Fullname' ] . '/' );
        else
            return( $aFile[ 'Fullname' ] );
    }

    /** Returns an array of fullnames (that is path and filename)
        return array of strings
    */
    Function Fullnames()
    {
        reset( $this->aFiles );
        foreach( $this->aFiles as $sKey => $aFile )
        	$aFiles[ $this->Fullname( $aFile ) ] = $this->FullName( $aFile );

        return( $aFiles );
    }

    /** Returns an array of filenames (that is filename with extension, but without path)
        return array of strings
    */
    Function Filenames()
    {
        reset( $this->aFiles );
        foreach( $this->aFiles as $sKey => $aFile )
        	$aFiles[ $this->Filename( $aFile ) ] = $this->Filename( $aFile );

        return( $aFiles );
    }

    /** Are filters casesensitiv?
    */
    Function SetCasesensitiv( $fValue )
    {
        $this->fCasesensitiv = $fValue;
    }
    Function GetCasesensitiv()
    {
        return( $this->fCasesensitiv );
    }

    /** Convert found file/directorynames into lowercase?
    */
    Function SetLowercase( $fValue )
    {
        $this->fLowercase = $fValue;
    }
    Function GetLowercase()
    {
        return( $this->fLowercase );
    }
}





###------------------------------------------------------------------
/*
* Text File Manipulation Class
* Author: Boris Penck <boris@gamate.com>
* Date: 2001-06-24
*/
class FileMan {

	var $basefile = "";
	var $filearray = array();
	var $totallines = 0;
	
	/*
	* setFile(string filename)
	* set filename for further use
	*/
	function setFile($file="") {
		if($file!="" && file_exists($file)) {
			$this->basefile = $file;
			$this->filearray = $this->readFileToArray();
			foreach($this->filearray as $key => $value) {
				$this->totallines++;
			}
		} 
	}

	// Do not call ! Internal function 
	function outputError($errorNo) {
		switch($errorNo) {
			case 1:
				echo '<b>Error</b>: No file selected. Define your file with $class->setFile(file) !';
			break;
			case 2:
				echo '<b>Error</b>: Selected File not found or bad file permissions !';
			break;
			case 3:
				echo '<b>Error</b>: File not modified or given line not found';
			break;
		
			default:
				echo '<b>Error</b>: Unkown error in Class FileMan';
		}
	}
	
	// Do not call ! Internal function
	function readFileToArray() {
		if($this->basefile!="") {
			$tempArray = file($this->basefile);
			return $tempArray;
		} else {
			$this->outputError(1);
		}
	}

	###---------------------------------------------------------------
	/*
	* Callable functions
	*
	* below this, there all all callable function to manipulate a file
	*
	*/
	###---------------------------------------------------------------
	
	
	/*
	* readEntire()
	* 	returns an array containing the complete file, each arrayelement
	* 	contains one line of the file
	*/
	function readEntire() {
		if($this->basefile!="") {
			return $this->filearray;
		} else {
			$this->outputError(1);
		}
	}
	
	/*
	* readFirstX(int amount of lines)
	*	returns an array containing the first X Lines
	*/
	function readFirstX($amountOfLines) {
		if($this->basefile!="") {
			$tempArray = array();
			for($line=0;$line<=($amountOfLines-1);$line++) {
				$tempArray[] = $this->filearray[$line];
			}
			return $tempArray;
		} else {
			$this->outputError(1);
		}
	}
	
	/*
	* readLastX(int amount of lines)
	*	returns an array contaiing the last X lines
	*/
	function readLastX($amountOfLines) {
		if($this->basefile!="") {
			$tempArray = array();
			$startLine = $this->totallines - $amountOfLines;
			$i=0;
			foreach($this->filearray as $key => $value) {
				if($i>=$startLine) {
					$tempArray[] = $value;
				}
				$i++;
			}
			return $tempArray;
		} else {
			$this->outputError(1);
		}
	}
	
	/*
	* writeEnd(string string)
	*	appends a line to the file
	*/
	function writeEnd($writeStr) {
		if($this->basefile!="") {
			$fp = fopen($this->basefile,"a");
			if($fp) {
				if(!ereg("\n$",$writeStr)) {
					$writeStr .= "\n";
				}
				fputs($fp,$writeStr);
				fclose($fp);
				return true;
			} else {
				$this->outputError(2);
			}
		} else {
			$this->outputError(1);
		}
	}

    /*
    * writebegin(string string)
    *       prepends a line to the file
    */
	function writeBegin($writeStr) {
		if($this->basefile!="") {
			$fp = @fopen($this->basefile,"w");
			if($fp) {
				if(!ereg("\n$",$writeStr)) {
					$writeStr .= "\n";
				}
				fputs($fp,$writeStr);
				foreach($this->filearray as $lNo => $lineValue) {
					fputs($fp,$lineValue);
				}
				fclose($fp);
				return true;
			} else {
				$this->outputError(2);
			}
		} else {
			$this->outputError(1);
		}
	}
	
    /*
    * delLineNo(int number of line)
    *       delete a line is the file, lines begin with 1 (not 0!)
    */
	function delLineNo($lineNo) {
		if($this->basefile!="") {
			$fp = @fopen($this->basefile,"w");
			if($fp) {
				foreach($this->filearray as $lNo => $lineValue) {
					if($lNo != ($lineNo-1)) {
						fputs($fp,$lineValue);
					} else {
						$modificated = 1;
					}
				}
				fclose($fp);
				if($modificated==1) {
					return true;
				} else {
					$this->outputError(3);
				}
			} else {
				$this->outputError(2);
			}
		} else {
			$this->outputError(1);
		}
	}
	
    /*
    * writeAfterLine(int line number, string string)
    *       insert a line after a given line number, lines begin with 1
    */
	function writeAfterLine($lineNo,$writeStr) {
		if($this->basefile!="") {
			$fp = @fopen($this->basefile,"w");
			if($fp) {
				if(!ereg("\n$",$writeStr)) {
					$writeStr .= "\n";
				}
				foreach($this->filearray as $lNo => $lineValue) {
					if($lNo==($lineNo-1)) {
						fputs($fp,$lineValue);
						fputs($fp,$writeStr);
						$modificated = 1;
					} else {
						fputs($fp,$lineValue);
					}
				}
				fclose($fp);
				if($modificated==1) {
					return true;
				} else {
					$this->outputError(3);
				}
			} else {
				$this->outputError(2);
			}
		} else {
			$this->outputError(1);
		}
	}
	
    /*
    * replaceLine(int number of line, string string)
    *       replaces a line with the given sting(line), lines begin with 1
    */
	function replaceLine($lineNo,$replaceStr) {
		if($this->basefile!="") {
			$fp = @fopen($this->basefile,"w");
			if($fp) {
				if(!ereg("\n$",$replaceStr)) {
					$replaceStr .= "\n";
				}
				foreach($this->filearray as $lNo => $lineValue) {
					if($lNo==($lineNo-1)) {
						fputs($fp,$replaceStr);
						$modificated = 1;
					} else {
						fputs($fp,$lineValue);
					}
				}
				fclose($fp);
				if($modificated==1) {
					return true;
				} else {
					$this->outputError(3);
				}
			} else {
				$this->outputError(2);
			}
		} else {
			$this->outputError(1);
		}
	}
	
    /*
    * getLine(int number of line)
    *       returns a string containing line X of the file, lines begin with 0!
    */
	function getLine($lineNo) {
		if($this->basefile!="") {
			return $this->filearray[$lineNo];
		} else {
			$this->outputError(1);
		}
	}
	
    /*
    * getLastLine(int number of line)
    *       returns a string containing the last line of the file
    */
	function getLastLine() {
		if($this->basefile!="") {
			return $this->filearray[$this->totallines];
		} else {
			$this->outputError(1);
		}
	}
	
    /*
    * getFirstLine(int number of line)
    *       returns a string containing the first line of the file
    */
	function getFirstLine() {
		if($this->basefile!="") {
			return $this->filearray[0];
		} else {
			$this->outputError(1);
		}
	}
	
    /*
    * getRandomLine()
    *       returns a string containing a random line of the file
    */
	function getRandomLine() {
		if($this->basefile!="") {
			$randInt = rand(0,$this->totallines);
			return $this->filearray[$randInt];
		} else {
			$this->outputError(1);
		}
	}
	
    /*
    * searchInLine(string string)
    *       searches each line for a string or regular expression and
	*    	returns an array of $linenumber => $linecontent
    */
	function searchInLine($sStr="") {
		$tempArray = array();
		foreach($this->filearray as $lineNo => $lineValue) {
			if(ereg($sStr,$lineValue)) {
				$tempArray[$lineNo] = $lineValue;
				$found = 1;
			}
		}
		if($found==1) {
			return $tempArray;
		} else {
			return false;
		}
	}
}

?>